<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => 'index', 'show']); 
    }

    public function index()
    {
        // $post = Post::all();
    return view('blog.index')
    ->with('posts', Post::orderBy('updated_at', 'DESC')->get());
}

    public function create()
    {
      return view('blog.create');
    }


    public function store(Request $request)
    {
       //Validacija
       $request->validate([
        'title' => 'required',
        'description' => 'required',
        'image' => 'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        // za image za zapishuvanje vo baza
        $newImageName = uniqid() . '-' . $request->title . '.' . $request->image->extension();
        $request->image->move(public_path('images'), $newImageName);
        // Za kreiranje na slug
        // $slug =SlugService::createSlug(Post::class, 'slug', $request->title);

        Post::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'slug' => SlugService::createSlug(Post::class, 'slug', $request->title),
            'image_path' => $newImageName,
            'user_id' => auth()->user()->id
        ]);
        // Poraka vo blog/index.blade.php - session
        return redirect ('/blog')->with('message', 'Vasiot post e zacuvan');
    }

       // Prikaz na eden blog spored $slug
    public function show($slug)
    {
       return view('blog.show')
       ->with('post', Post::where('slug', $slug)->first());
    }


    public function edit($slug)
    {
        return view('blog.edit')
        ->with('post', Post::where('slug', $slug)->first());
    }


    public function update(Request $request, $slug)
    {
               //Validacija
       $request->validate([
        'title' => 'required',
        'description' => 'required',
        ]);

        Post::where('slug', $slug)
        ->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'slug' => SlugService::createSlug(Post::class, 'slug', $request->title),
            'user_id' => auth()->user()->id
        ]);
        return redirect('/blog')
        ->with('message', 'Tvojot post e uspesno apdajtiran');
    }


    public function destroy($slug)
    {
        $post = Post::where('slug', $slug);
        $post->delete();
        return redirect('/blog')
        ->with('message', 'Postot e izbrisan');
    }

}
