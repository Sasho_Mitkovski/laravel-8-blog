@extends('layouts.app')
@section('content')
    <div class="background-image grid grid-cols-1 m-auto">
        <div class="flex text-gray-100 pt-10">
            <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block text-center">
                <h2 class="sm:text-white text-5xl uppercase font-bold text-shadow-md pb-14">
                    Дали сакате да станете програмер?
                </h2>
                <a href="/blog" class="text-center bg-gray-50 text-gray-700 py-2 px-4 font-bold text-xl uppercase">
                    Прочитај повеќе</a>
            </div>
        </div>
    </div>
    <div class="sm:grid grid-cols-2 gap-20 w-4/5 mx-auto py-8 border-b border-gray-200">
        <img src="https://cdn.pixabay.com/photo/2017/06/08/07/25/bear-2382779__340.jpg" width="500" alt="">
        <div class="m-auto sm:m-auto text-left w-4/5 block">
            <h2 class="text-4xl font-extrabold text-gray-600">
                Се борите да бидете подобар веб програмер?
            </h2>
            <p class="py-8 text-gray-500 text-l">
                Графикот покажува како вработените најчесто напредуваат во својата кариера. Кликнете на името на позицијата
                за
                да добиете детали со податоци за плата
            </p>
            <p class="font-extrabold text-gray-600 text-s pb-9">
                Плата за работната позиција според регион, образование, работно искуство, големина на компанија и возраст
            </p>
            <a href="/blog" class="uppercase bg-blue-500 text-gray-100 text-s font-extrabold py-3 px-8 rounded-3xl">
                Дознај повеќе</a>
        </div>
    </div>
    <div class="text-center p-15 bg-black text-white">
        <h2 class="text-2xl pb-5 text-l">
            Јас сум експерт за....
        </h2>
        <span class="font-extrabold block text-4xl py-1">
            Ux Design
        </span>
        <span class="font-extrabold block text-4xl py-1">
            Project Manager
        </span>
        <span class="font-extrabold block text-4xl py-1">
            Digital Strategy
        </span>
        <span class="font-extrabold block text-4xl py-1">
            Backend Development
        </span>
    </div>
    <div class="text-center py-15">
        <span class="uppercase  text-s text-gray-400">
            Blog
        </span>
        <h2 class="text-4xl font-bold py-8">
            Најнов пост
        </h2>
        <p class="m-auto w-4/5 text-gray-500">
            Анкетата покажа голема полова разлика во оваа дејност - 80.96% машки наспроти само 19.04% претставници од женскиот пол.
        </p>
    </div>
    <div class="sm:grid grid-cols-2 w-4/5 m-auto">
        <div class="flex bg-yellow-700 text-gray-100 pt-10">
            <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block">
                <span class="uppercase text-xs">
                    PHP
                </span>
                <h3 class="text-xl font-bold py-10">
                    64% од програмерите се класифицираат на позиција Middle и Senior според искуство (32.71% Middle, 31.31% Senior)
                </h3>
                <a href="/blog" class="uppercase bg-transparent border-2 border-gray-100 text-gray-100 text-xs font-extrabold py-3 px-8 rounded-3xl">
                    Дознај повеќе</a>
            </div>
        </div>
        <div>
            <img src="https://cdn.pixabay.com/photo/2017/06/08/07/25/bear-2382779__340.jpg" width="500" alt="">
        </div>
    </div>
@endsection
